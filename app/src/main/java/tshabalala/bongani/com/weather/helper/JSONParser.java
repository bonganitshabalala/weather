package tshabalala.bongani.com.weather.helper;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

/**
 * Created by Bongani on 2017/02/18.
 */
public class JSONParser {

    public JSONParser()
    {}

    public String makeHttpRequest(String strUrl)
    {
        StringBuilder stringBuilder = new StringBuilder();
        String line = "";
        try
        {
            URL url = new URL(strUrl);
            //opening the connection
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            //read the response
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            try {
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }catch (Exception ex)
        {

        }

        return stringBuilder.toString();
    }

}



