package tshabalala.bongani.com.weather;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.test.mock.MockPackageManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import tshabalala.bongani.com.weather.helper.JSONParser;
import tshabalala.bongani.com.weather.helper.LocaWeather;
import tshabalala.bongani.com.weather.helper.Weather;

public class MainActivity extends AppCompatActivity {

    private LocationManager locationMangaer = null;
    private LocationListener locationListener = null;
    String path = "http://openweathermap.org/img/w/";
    private static final int REQUEST_CODE_PERMISSION = 1;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;

    //********************************************************************************************8

    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    private TextView txtLocation, txtTemp, txtDeg, txtForecast, txtTimeUpdated, txtWind, txtHumidity, txtOther;
    private String strLocation = "";
    private String url;
    private static final String TAG = "Debug";
    private Weather weather, weatherCon;
    private LocaWeather locaWeather;
    private ImageView img;
    String image;
    private Boolean flag = false;
    private String longi = null;
    String lati = null;


    //**********************************************************************************************


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!isNetworkConnected(this)) {
            alertbox("Alert", "No internet connection- check network settings");

        } else {


            flag = displayGpsStatus();
            if (flag) {

                locationMangaer = (LocationManager) getSystemService(this.LOCATION_SERVICE);
                locationListener = new mylocationlistener();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Alert")
                        .setCancelable(false)
                        .setTitle("GPS not set - enable location")
                        .setPositiveButton("Open settings",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();

            }
            url = "http://api.openweathermap.org/data/2.5/weather?lat=35&lon=139&APPID=53f9d8e4213222cf517d86dc406d67fc";

            new retrieveData().execute();

            //Toast.makeText(MainActivity.this, "connected ", Toast.LENGTH_LONG).show();
        }


        // check if GPS enabled


        txtLocation = (TextView) findViewById(R.id.textViewLocation);
        txtTemp = (TextView) findViewById(R.id.textViewTemp);
        txtForecast = (TextView) findViewById(R.id.textViewForecast);
        img = (ImageView) findViewById(R.id.imageView);
        txtTimeUpdated = (TextView) findViewById(R.id.textViewUpdated);

        txtHumidity = (TextView) findViewById(R.id.textViewHumidity);

        txtWind = (TextView) findViewById(R.id.textViewWind);


    }


    /*----------Listener class to get coordinates ------------- */


    private class mylocationlistener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {

                lati = location.getLatitude() + "";
                longi = location.getLongitude() + "";
                Log.w(TAG, "long " + longi + " ca " + lati);


            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    protected void alertbox(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton("Reload",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                                startActivity(getIntent());
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private boolean isNetworkConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }



    private class retrieveData extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Retrieving location details...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {


            Log.w(TAG,"long "+ longi+ " ca "+lati);


            String json = jsonParser.makeHttpRequest(url);


            Log.e("TAG", "Response from url: " + json.toString());

            if (json != null) {
                try {
                    JSONObject jsonObj = new JSONObject(json);
                    // Getting JSON Array node


                    JSONObject coordObj = jsonObj.getJSONObject("coord");
                    locaWeather = new LocaWeather().setLatitude(coordObj.getString("lat"))
                            .setLongitude(coordObj.getString("lon"));
                    Log.e("TAG", "coord: " + locaWeather.getLatitude()+","+locaWeather.getLongitude());

                    JSONObject sysObj = jsonObj.getJSONObject("sys");
                    locaWeather = new LocaWeather().setCountry(sysObj.getString("country"))
                            .setSunrise(sysObj.getString("sunrise"))
                            .setSunset(sysObj.getString("sunset"));

                    JSONArray jArr = jsonObj.getJSONArray("weather");
                    Log.e("TAG", "att: " + jArr.toString());
                    JSONObject jsonWeather = jArr.getJSONObject(0);
                    Log.e("TAG", "atr: " + jsonWeather.toString());
                    weather =  new Weather().setId(jsonWeather.getString("id"))
                            .setMain(jsonWeather.getString("main"))
                            .setDescription(jsonWeather.getString("description"))
                            .setIcon(jsonWeather.getString("icon"));
                    Log.e("TAG", "weather: " + weather.getDescription());


                    JSONObject mainObj = jsonObj.getJSONObject("main");
                    weather =  new Weather().setTemp(mainObj.getString("temp"))
                            .setHumidity(mainObj.getString("humidity"))
                            .setPressure(mainObj.getString("pressure"))
                            .setTemp_min(mainObj.getString("temp_min"))
                            .setTemp_max(mainObj.getString("temp_max"));
                   // Log.e("TAG", "weather: " + weather.getTemp()+","+weather.getHumidity()+","+weather.getPressure());

// Wind
                  //  JSONObject windObj = jsonObj.getJSONObject("wind");
                 //   weather = new Weather().setSpeed(windObj.getString("speed"))
                   //         .setDeg(windObj.getString("deg"));

// Clouds
                    JSONObject cloudsObj = jsonObj.getJSONObject("clouds");
                    weather = new Weather().setCovered(cloudsObj.getString("all"));


                    String dt = jsonObj.getString("dt");
                    String strName = jsonObj.getString("name");
                    String number = jsonObj.getString("id");
                    String cod = jsonObj.getString("cod");


                    weather =  new Weather().setDt(dt)
                            .setId_num(number)
                            .setName(strName)
                            .setCod(cod);

                     image = path + jsonWeather.getString("icon");
                    Log.w("TAG",""+image);

                    weatherCon = new Weather(jsonWeather.getString("description"),image,mainObj.getString("temp"),mainObj.getString("humidity"),mainObj.getString("pressure"));
                    Log.e("TAG", "weather: " + weather.getDt()+","+weather.getName()+","+weather.getIcon());


                } catch (final JSONException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e("TAG", "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Couldn't get json from server.",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                Log.e("TAG", "weather: " + weather.getDescription() + "," + weather.getMain() + "," + weather.getDescription() + "," + weather.getIcon());

                txtLocation.setText(weather.getName() + " , " + locaWeather.getCountry());

                if (weatherCon != null) {
                    float celc = Float.parseFloat(weatherCon.getTemp()) - 273;
                    NumberFormat formatter = new DecimalFormat("#0");
                    txtTemp.setText(formatter.format(celc) + " `C");

                    txtForecast.setText(weatherCon.getDescription());


                    txtWind.setText("Pressure " + weatherCon.getPressure());
                    txtHumidity.setText("Humidity " + weatherCon.getHumidity() + " %");

                    // img.setImageResource(Integer.parseInt(weather.getIcon()));
                    Picasso.with(MainActivity.this).load(image + ".png").into(img);

                }
                long unixSeconds = Long.parseLong(weather.getDt());
                Date date = new Date(unixSeconds * 1000L);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+2"));
                String formattedDate = sdf.format(date);
                txtTimeUpdated.setText("Updated as of " + formattedDate);


            }catch (Exception e)
            {
                e.getStackTrace();
            }


        }




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId())
        {
            case R.id.action_refresh:

                txtLocation.setText("");
                txtTemp.setText("");
                txtTimeUpdated.setText("");
                txtForecast.setText("");
                txtWind.setText("");
                txtHumidity.setText("");
                finish();
                startActivity(getIntent());
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }



}
