package tshabalala.bongani.com.weather.helper;

/**
 * Created by Bongani on 2017/02/18.
 */
public class LocaWeather {

    public LocaWeather() {
    }

    private String longitude;
    private String latitude;
    private String sunset;
    private String sunrise;
    private String country;

    public String getLongitude() {
        return longitude;
    }

    public LocaWeather setLongitude(String longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getLatitude() {
        return latitude;
    }

    public LocaWeather setLatitude(String latitude) {
        this.latitude = latitude;
        return this;
    }

    public String getSunset() {
        return sunset;
    }

    public LocaWeather setSunset(String sunset) {
        this.sunset = sunset;
        return this;
    }

    public String getSunrise() {
        return sunrise;
    }

    public LocaWeather setSunrise(String sunrise) {
        this.sunrise = sunrise;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public LocaWeather setCountry(String country) {
        this.country = country;
        return this;
    }



}
