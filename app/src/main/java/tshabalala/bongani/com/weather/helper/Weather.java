package tshabalala.bongani.com.weather.helper;

/**
 * Created by Bongani on 2017/02/18.
 */
public class Weather {

    private String id;
    private String main;
    private String description;
    private String icon;

    private String temp;
    private String humidity;
    private String pressure;
    private String temp_min;
    private String temp_max;

    private String speed;
    private String deg;

    private String rain;

    private String time;

    private String covered;

    private String dt;
    private String id_num;

    public String getRain() {
        return rain;
    }

    public Weather(String description, String icon, String temp,String humidity,String pressure) {
        this.description = description;
        this.icon = icon;
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
    }

    public Weather setRain(String rain) {
        this.rain = rain;
        return this;
    }

    public String getTemp() {
        return temp;
    }

    public Weather setTemp(String temp) {
        this.temp = temp;
        return this;
    }

    public String getCod() {
        return cod;
    }

    public Weather setCod(String cod) {
        this.cod = cod;
        return this;
    }

    public String getName() {
        return name;
    }

    public Weather setName(String name) {
        this.name = name;
        return this;
    }

    public String getId_num() {
        return id_num;
    }

    public Weather setId_num(String id_num) {
        this.id_num = id_num;
        return  this;
    }

    public String getDt() {
        return dt;
    }

    public Weather setDt(String dt) {
        this.dt = dt;
        return this;
    }

    public String getCovered() {
        return covered;
    }

    public Weather setCovered(String covered) {
        this.covered = covered;
        return this;
    }

    public String getTime() {
        return time;
    }

    public Weather setTime(String time) {
        this.time = time;
        return this;
    }

    public String getDeg() {
        return deg;
    }

    public Weather setDeg(String deg) {
        this.deg = deg;
        return this;
    }

    public String getSpeed() {
        return speed;
    }

    public Weather setSpeed(String speed) {
        this.speed = speed;
        return this;
    }

    public String getTemp_max() {
        return temp_max;
    }

    public Weather setTemp_max(String temp_max) {
        this.temp_max = temp_max;
        return this;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public Weather setTemp_min(String temp_min) {
        this.temp_min = temp_min;
        return this;
    }

    public String getPressure() {
        return pressure;
    }

    public Weather setPressure(String pressure) {
        this.pressure = pressure;
        return this;
    }

    public String getHumidity() {
        return humidity;
    }

    public Weather setHumidity(String humidity) {
        this.humidity = humidity;
        return this;
    }

    private String name;
    private String cod;




    public Weather() {
    }

    public String getId() {
        return id;
    }

    public Weather setId(String id) {
        this.id = id;
        return this;
    }

    public String getMain() {
        return main;
    }

    public Weather setMain(String main) {
        this.main = main;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Weather setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getIcon() {
        return icon;
    }

    public Weather setIcon(String icon) {
        this.icon = icon;
        return this;
    }
}
